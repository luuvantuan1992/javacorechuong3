package lesson2;

import java.util.Scanner;

public class DienTichHinhChuNhat {
	protected int chieuDai, chieuRong;

	public static double tinhDienTich(double chieuDai, double chieuRong) {
		return chieuDai * chieuRong;
	}

	public static void main(String[] args) {
		double dai, rong;
		Scanner scanner = new Scanner(System.in);
		System.out.println("Nhập chiều dài : ");
		dai = scanner.nextDouble();
		System.out.println("Nhập chiều rộng : ");
		rong = scanner.nextDouble();
		System.out.println("Diện Tích hình chữ nhật là " + tinhDienTich(dai, rong));
	}
}
