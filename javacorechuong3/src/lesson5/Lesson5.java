package lesson5;

public class Lesson5 {
	// Ham tinh Giai thua
	public static long tinhGiaiThua(int n) {
		long gt = 1;
		for (int i = 1; i <= n; i++) {
			gt *= i;
		}
		return gt;
	}

	public static void main(String[] args) {
		long S = 0;
		for (int i = 1; i <= 7; i++) {
			S += tinhGiaiThua(i);
		}
		System.out.println("Tong cac giai thua la: " + S);
	}

}
