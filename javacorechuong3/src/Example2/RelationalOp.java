package Example2;

public class RelationalOp {
	public static void main(String args[]) {
		float a = 10.0F;
		double b = 10.0;
		if (a == b)
			System.out.println("a and b are equal");
		else
			System.out.println("a and b are not equal");
	}
}
